# API-Platform - Gitlab-CI

The goal here is to setup a CI pipeline for API-Platfrom using Docker and Gitlab.
## Getting Started

### Prerequisities

In order to run this container you'll need docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

You will also need a Gitlab account :

* [Gitlab](https://about.gitlab.com)

Finally, `git` :

* [git](https://git-scm.com/)

### Cloning the repo

`git clone https://gitlab.com/riipeckx/alternance-devops.git`

### Usage

#### Building images

In order to build dev env:
`./build-dev.sh`

or build prod env:
`./build-prod.sh`
#### Running services

`docker-compose up -d`
#### Environment Variables

* `APP_ENV` - Defines the environment the PHP Server will run into.

## Find Us

* [homepage](https://riipeckx.io)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the 
[tags on this repository](https://github.com/your/repository/tags). 

## Authors

* **Victor MERCIER** - *Initial work* - [riipeckx](https://riipeckx.io)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.