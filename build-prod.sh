#!/bin/bash
docker build -t riipeckx/alternance-devops-php:prod ./api/.
docker build -t riipeckx/alternance-devops-pwa:prod ./pwa/.
docker build -t riipeckx/alternance-devops-caddy:prod ./api/.